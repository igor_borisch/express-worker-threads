import { Base64 } from 'js-base64';
import { prepareOfferData } from './prepareOfferData';

const js2xmlparser = require('js2xmlparser');

export const prepareObipRequestXml = (confidant, borrowerName, maxOnclearedSum, oliverWymanPoints, config) => {
    let offerData = prepareOfferData(confidant, borrowerName, maxOnclearedSum, oliverWymanPoints);

    offerData = Base64.encode(
        js2xmlparser.parse('data', offerData, { declaration: { encoding: 'UTF-8' } })
            .replace(/row\d+/g, 'row') // теги продуктов должны быть row, но с одинаковыми ключами объект создавать нельзя
    );

    return (
        `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v2="http://xmlns.oracle.com/oxp/service/v2">
          <soapenv:Header/>
          <soapenv:Body>
             <v2:runReport>
                <v2:reportRequest>
                   <v2:attributeFormat>pdf</v2:attributeFormat>
                   <v2:attributeLocale>ru-RU</v2:attributeLocale>
                   <v2:flattenXML>true</v2:flattenXML>
                   <v2:reportAbsolutePath>${config.services.reportService.obipTemplatePath}</v2:reportAbsolutePath>
                        <v2:reportData>${offerData}</v2:reportData>
                   <v2:sizeOfDataChunkDownload>-1</v2:sizeOfDataChunkDownload>
                </v2:reportRequest>
                <v2:userID>${config.services.reportService.obipUserId}</v2:userID>
                <v2:password>${config.services.reportService.obipPassword}</v2:password>
             </v2:runReport>
          </soapenv:Body>
        </soapenv:Envelope>`
    );
};

