export const prepareKkFileRequestXml = (limitRequestNumber, data, config) => (
    `
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:flm="http://alfabank.ru/FLM.Endpoints" xmlns:lm="http://alfabank.ru/LM.Endpoints">
            <soapenv:Header>
                <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                    <wsse:UsernameToken>
                        <wsse:Username>${config.services.saveFileService.wcfUserName}</wsse:Username>
                        <wsse:Password>${config.services.saveFileService.wcfPassword}</wsse:Password>
                    </wsse:UsernameToken>
                </wsse:Security>
            </soapenv:Header>
            <soapenv:Body>
                <flm:put>
                    <flm:systemCode>CB</flm:systemCode>      
                    <flm:externalId>${limitRequestNumber}</flm:externalId>
                    <flm:file>
                        <lm:name>Коммерческое предложение.pdf</lm:name>
                        <lm:kindCode>CorporateApprovals</lm:kindCode>
                        <lm:content>${data}</lm:content>
                        <lm:base64>true</lm:base64>
                    </flm:file>
                    <flm:comment>Сгенерировано веб-приложением "Онлайн-кредитование"</flm:comment>
                </flm:put>
            </soapenv:Body>
        </soapenv:Envelope>
`
);
