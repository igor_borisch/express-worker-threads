export const calculateMaxOnlcredSum = (oliverWymanPoints, annualRevenue) => {
    oliverWymanPoints = parseInt(oliverWymanPoints, 10);
    annualRevenue = annualRevenue.replace(/\s+/g, '');
    annualRevenue = annualRevenue.replace(/[,]/, '.');
    if (oliverWymanPoints <= 10) {
        return (parseFloat(annualRevenue) * 0.05).toFixed(2);
    }
    if (oliverWymanPoints === 11) {
        return (parseFloat(annualRevenue) * 0.045).toFixed(2);
    }
    if (oliverWymanPoints === 12) {
        return (parseFloat(annualRevenue) * 0.04).toFixed(2);
    }
    if (oliverWymanPoints === 13) {
        return (parseFloat(annualRevenue) * 0.03).toFixed(2);
    }

    return 'скоринговый балл больше 13, заявка не может быть оформлена';
};
