const validateOliverWymanPoints = (oliverWymanPoints, validationsResult) => {
    // eslint-disable-next-line no-empty
    if (oliverWymanPoints > 0 && oliverWymanPoints < 14) {
    } else {
        validationsResult.push('Некорректный скорбалл');
    }
};

const validateINN = (inn, validationsResult) => {
    inn = `${inn}`;
    inn = inn.split('');

    if ((inn.length === 10) &&
        (parseInt(inn[9], 10) === (
            (
                2 *
                inn[0] +
                4 * inn[1] +
                10 * inn[2] +
                3 * inn[3] +
                5 * inn[4] +
                9 * inn[5] +
                4 * inn[6] +
                6 * inn[7] +
                8 * inn[8]) % 11) % 10
    // eslint-disable-next-line no-empty
        )) {
    } else {
        validationsResult.push('Некорректный ИНН');
    }
};

const validateLimitRequestNumber = (limitRequestNumber, validationsResult) => {
    if (
        parseInt(limitRequestNumber, 10) > 0 &&
        parseInt(limitRequestNumber, 10) < 2147483647
    // eslint-disable-next-line no-empty
    ) {
    } else {
        validationsResult.push('Некорректный номер заявки на лимит');
    }
};

const validateAnnualRevenue = (annualRevenue, validationsResult) => {
    // eslint-disable-next-line no-empty
    if (parseInt(annualRevenue, 10) > 0) {
    } else {
        validationsResult.push('Некорректная сумма');
    }
};

export const validateRequest = (limitRequestNumber, oliverWymanPoints, annualRevenue, inn) => {
    // eslint-disable-next-line prefer-const
    let validationsResult = [];

    validateLimitRequestNumber(limitRequestNumber, validationsResult);
    validateOliverWymanPoints(oliverWymanPoints, validationsResult);
    validateAnnualRevenue(annualRevenue, validationsResult);
    validateINN(inn, validationsResult);

    const hasError = !!(
        validationsResult[0] ||
        validationsResult[1] ||
        validationsResult[2] ||
        validationsResult[3]
    );

    return { validationsResult, hasError };
};
