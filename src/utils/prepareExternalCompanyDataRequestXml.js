export const prepareRequestXml = (inn, config) => (
    `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsex="http://WSExternalCompanyData10.EQ.CS.ws.alfabank.ru">
            <soapenv:Body>
                <wsex:WSExternalCompanyDataGetUploadInfo>
                    <inCommonParms>
                        <userID>${config.services.externalCompanyDataService.wsEQuserId}</userID>
                        <externalSystemCode>${config.services.externalCompanyDataService.wsEQexternalSystemCode}</externalSystemCode>
                        <externalUserCode>${config.services.externalCompanyDataService.wsEQexternalUserCode}</externalUserCode>
                    </inCommonParms>
                    <inParms>
                        <parms><![CDATA[<filter><group>COMPHEADS</group><inn>${inn}</inn></filter>]]></parms>
                    </inParms>
                </wsex:WSExternalCompanyDataGetUploadInfo>
            </soapenv:Body>
        </soapenv:Envelope>`
);
