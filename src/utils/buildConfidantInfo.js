export const buildConfidantInfo = (data) => {
    const confidantInfo = data
        .COMPHEADS
        .EGRUL
        .LER_BASEINFO
        .LER_AUTHPERSONS_LIST
        .LER_AUTHPERSONS;

    const confidant = `${confidantInfo.AUTHPERSON_FAMILY_NAME || ''
    } ${
        confidantInfo.AUTHPERSON_FIRST_NAME || ''
    } ${
        confidantInfo.AUTHPERSON_PATRONYMIC_NAME || ''}`;

    const borrowerName = data.COMPHEADS.EGRUL.LER_BASEINFO.LE_FULL_NAME;

    return { confidant, borrowerName };
};
