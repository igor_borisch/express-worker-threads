const oliverWyman10 = (confidant, borrowerName, maxOnclearedSum) => ({
    onlcredNamePerson: confidant,
    onlcredNameCompany: borrowerName,
    onlcredSum: maxOnclearedSum,
    Product: {
        row1: {
            ProductName: 'Возобновляемая кредитная линия',
            ProductTime: '12 мес.',
            ProductGraph: 'Транши до 6 мес.',
            ProductPercent: '1,5% в мес.'
        },
        row2: {
            ProductName: 'Овердрафт',
            ProductTime: '12 мес.',
            ProductGraph: 'транши до 3 мес. или лимит с обнулением каждые 3 мес',
            ProductPercent: '1,5% в мес.'
        },
        row3: {
            ProductName: 'Кредит',
            ProductTime: '12 мес.',
            ProductGraph: 'Ежемесячно или ежеквартально',
            ProductPercent: '1,5% в мес.'
        },
        row4: {
            ProductName: 'Гарантия',
            ProductTime: 'до 18 мес.',
            ProductGraph: '-',
            ProductPercent: '5% за выдачу (не менее 65 тыс. рублей)'
        }
    }
});

const oliverWyman11 = (confidant, borrowerName, maxOnclearedSum) => ({
    onlcredNamePerson: confidant,
    onlcredNameCompany: borrowerName,
    onlcredSum: maxOnclearedSum,
    Product: {
        row1: {
            ProductName: 'Возобновляемая кредитная линия',
            ProductTime: '12 мес.',
            ProductGraph: 'Транши до 6 мес.',
            ProductPercent: '1,5% в мес.'
        },
        row2: {
            ProductName: 'Овердрафт',
            ProductTime: '12 мес.',
            ProductGraph: 'транши до 3 мес. или лимит с обнулением каждые 3 мес',
            ProductPercent: '1,5% в мес.'
        },
        row3: {
            ProductName: 'Кредит',
            ProductTime: '12 мес.',
            ProductGraph: 'Ежемесячно или ежеквартально',
            ProductPercent: '1,5% в мес.'
        },
        row4: {
            ProductName: 'Гарантия',
            ProductTime: 'до 18 мес.',
            ProductGraph: '-',
            ProductPercent: '5% за выдачу (не менее 65 тыс. рублей)'
        }
    }
});

const oliverWyman12 = (confidant, borrowerName, maxOnclearedSum) => ({
    onlcredNamePerson: confidant,
    onlcredNameCompany: borrowerName,
    onlcredSum: maxOnclearedSum,
    Product: {
        row1: {
            ProductName: 'Возобновляемая кредитная линия',
            ProductTime: '12 мес.',
            ProductGraph: 'Транши до 2 мес.',
            ProductPercent: '1,5% в мес.'
        },
        row2: {
            ProductName: 'Овердрафт',
            ProductTime: '12 мес.',
            ProductGraph: 'транши до 2 мес. или лимит с обнулением каждые 2 мес',
            ProductPercent: '1,5% в мес.'
        },
        row3: {
            ProductName: 'Кредит',
            ProductTime: '9 мес.',
            ProductGraph: 'Ежемесячно или ежеквартально',
            ProductPercent: '1,5% в мес.'
        },
        row4: {
            ProductName: 'Гарантия',
            ProductTime: 'до 18 мес.',
            ProductGraph: '-',
            ProductPercent: '5% за выдачу (не менее 65 тыс. рублей)'
        }
    }
});

const oliverWyman13 = (confidant, borrowerName, maxOnclearedSum) => ({
    onlcredNamePerson: confidant,
    onlcredNameCompany: borrowerName,
    onlcredSum: maxOnclearedSum,
    Product: {
        row1: {
            ProductName: 'Возобновляемая кредитная линия',
            ProductTime: '12 мес.',
            ProductGraph: 'Транши до 2 мес.',
            ProductPercent: '1,5% в мес.'
        },
        row2: {
            ProductName: 'Овердрафт',
            ProductTime: '12 мес.',
            ProductGraph: 'транши до 2 мес. или лимит с обнулением каждые 2 мес',
            ProductPercent: '1,5% в мес.'
        },
        row3: {
            ProductName: 'Кредит',
            ProductTime: '6 мес.',
            ProductGraph: 'Ежемесячно или ежеквартально',
            ProductPercent: '1,5% в мес.'
        },
        row4: {
            ProductName: 'Гарантия',
            ProductTime: 'до 12 мес.',
            ProductGraph: '-',
            ProductPercent: '5% за выдачу (не менее 65 тыс. рублей)'
        }
    }
});

export const prepareOfferData = (confidant, borrowerName, maxOnclearedSum, oliverWymanPoints) => {
    switch (oliverWymanPoints) {
        case '11':
            return oliverWyman11(confidant, borrowerName, maxOnclearedSum);
        case '12':
            return oliverWyman12(confidant, borrowerName, maxOnclearedSum);
        case '13':
            return oliverWyman13(confidant, borrowerName, maxOnclearedSum);
        default:
            return oliverWyman10(confidant, borrowerName, maxOnclearedSum);
    }
};
