import request from 'request';
import { calculateMaxOnlcredSum } from '../utils/calculateMaxOnlcredSum';
import { prepareObipRequestXml } from '../utils/prepareObipRequestXml';

const xml2js = require('xml2js');

const parser = new xml2js.Parser({ explicitArray: false, trim: true });

export const runReportService = (data, config) => new Promise((resolve, reject) => {
    const {
        oliverWymanPoints,
        annualRevenue,
        borrowerName,
        confidant
    } = data;
    const maxOnclearedSum = calculateMaxOnlcredSum(oliverWymanPoints, annualRevenue);
    const xml = prepareObipRequestXml(confidant, borrowerName, maxOnclearedSum, oliverWymanPoints, config);
    const options = {
        url: config.services.reportService.url,
        method: 'POST',
        body: xml,
        headers: {
            SOAPAction: ''
        }
    };

    const callback = (error, response, body) => {
        if (!error && response.statusCode === 200) {
            parser.parseString(body, (err, result) => {
                resolve(result['soapenv:Envelope']['soapenv:Body'].runReportResponse.runReportReturn.reportBytes);
            });
        } else {
            parser.parseString(body, (err, result) => {
                reject(
                    result['soapenv:Envelope']['soapenv:Body']['soapenv:Fault']
                );
            });
        }
    };

    request(options, callback);
});

