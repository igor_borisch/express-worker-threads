import request from 'request';
import { prepareKkFileRequestXml } from '../utils/prepareKkFileRequestXml';

const xml2js = require('xml2js');

const parser = new xml2js.Parser({ explicitArray: false, trim: true });

export const saveFileServise = (limitRequestNumber, report, config) => new Promise((resolve, reject) => {
    const xml = prepareKkFileRequestXml(limitRequestNumber, report, config);
    const options = {
        url: config.services.saveFileService.url,
        method: 'POST',
        body: xml,
        headers: {
            'Content-Type': 'text/xml;charset=UTF-8',
            SOAPAction: 'put'
        }
    };

    const callback = (error, response, body) => {
        if (!error && response.statusCode === 200) {
            parser.parseString(body, (err, result) => {
                resolve(result);
            });
        } else {
            parser.parseString(body, (err, result) => {
                reject(result);
            });
        }
    };

    request(options, callback);
});

