import request from 'request';
import { prepareRequestXml } from '../utils/prepareExternalCompanyDataRequestXml';
import { buildConfidantInfo } from '../utils/buildConfidantInfo';

const xml2js = require('xml2js');

const parser = new xml2js.Parser({ explicitArray: false, trim: true });

export const getCompanyDataService = (inn, config) => new Promise((resolve, reject) => {
    const xml = prepareRequestXml(inn, config);
    const options = {
        url: config.services.externalCompanyDataService.url,
        method: 'POST',
        body: xml
    };
    const callback = (error, response, body) => {
        if (!error && response.statusCode === 200) {
            parser.parseString(body, (err, result) => {
                parser.parseString(
                    result['soapenv:Envelope']['soapenv:Body']['ns3:WSExternalCompanyDataGetUploadInfoResponse']
                        .response
                        .outParms
                        .resultSet
                        .resultSetRow
                        .data,
                    (err, result) => { // body ответа не распарсивается с 1 прохода
                        resolve(buildConfidantInfo(result));
                    }
                );
            });
        } else {
            parser.parseString(body, (err, result) => {
                reject(
                    result['soapenv:Envelope']['soapenv:Body']['soapenv:Fault']
                );
            });
        }
    };

    request(options, callback);
});

