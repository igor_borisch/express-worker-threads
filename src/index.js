import express from 'express';
import bodyParser from 'body-parser';
import { createCommercialOffer } from './controllers/createCommercialOffer';
import { healthmonitor } from './controllers/healthmonitor';
import { workerThreads } from "./controllers/asyncMultithread";
import setConfig from '../config/default';

const app = express();

app.use(bodyParser.json({ limit: '50mb' }));

const startup = async () => {
    const config = await setConfig();
    // eslint-disable-next-line no-console
    console.log(config);
    workerThreads(app);
    createCommercialOffer(app, config);
    healthmonitor(app, config);
    app.listen(config.serverPort, () => {
        console.log(`${config.startMessage} ${config.serverPort}`);
    });
};

startup();
