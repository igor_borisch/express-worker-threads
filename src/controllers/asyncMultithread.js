// eslint-disable-next-line import/no-unresolved
const { Worker, workerData } = require('worker_threads');
const sorter = require('./list-sorter');

const processor = () => {
    return new Promise((resolve, reject) => {
        const w = new Worker('./src/controllers/service.js', { workerData: [1, 2, 3, 4] });

        w.on('message', (msg) => {
            console.log(msg);
            resolve(msg);
        });
        w.on('error', console.error);
        w.on('exit', (code) => {
            if (code !== 0) {
                console.error(new Error(`Worker stopped with exit code ${code}`));
                reject(new Error(`Worker stopped with exit code ${code}`));
            }
        });
    });
};

export const workerThreads = (app) => {
    app.get('/workerThreads', async (req, res) => {
        console.log('get req', new Date());
        res.send(await processor());
    });
};
