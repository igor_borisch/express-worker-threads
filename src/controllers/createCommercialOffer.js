import { getCompanyDataService } from '../services/wsExternalCompanyDataService';
import { runReportService } from '../services/kkReportService';
import { saveFileServise } from '../services/skpSaveFileService';
import { validateRequest} from '../utils/validateRequest';

export const createCommercialOffer = (app, config) => {
    app.post('/createCommercialOffer', async (req, res) => {
        try {
            const {
                limitRequestNumber,
                oliverWymanPoints,
                annualRevenue,
                inn
            } = req.body;
            const validationResult = validateRequest(limitRequestNumber,
                oliverWymanPoints,
                annualRevenue,
                inn);

            if (validationResult.hasError) {
                res.status(400).send(validationResult);
            } else {
                const { confidant, borrowerName } = await getCompanyDataService(
                    inn,
                    config
                );
                const report = await runReportService(
                    {
                        oliverWymanPoints,
                        annualRevenue,
                        borrowerName,
                        confidant
                    },
                    config
                );

                await saveFileServise(limitRequestNumber, report, config);
                const response = {
                    ...req.body,
                    borrowerName,
                    confidant,
                    creationDate: new Date()
                };

                res.send(response);
            }

        } catch (e) {
            if (e && e.faultstring === 'DCS0012 По запросу не найдено ни одной компании') {
                res.status(406).send(e);
            } else {
                res.status(500).send(e);
            }
        }
    });
};
