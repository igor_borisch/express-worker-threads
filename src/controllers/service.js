const { workerData, parentPort } = require('worker_threads');

function random(min, max) {
    return Math.random() * (max - min) + min;
}

const sorter = require('./list-sorter');

const start = Date.now();
const bigList = Array(1000000).fill().map((_) => random(1, 10000));

sorter.sort(bigList);
parentPort.postMessage({ val: sorter.firstValue, timeDiff: Date.now() - start });
