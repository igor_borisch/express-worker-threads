export const healthmonitor = (app) => {
    app.get('/healthmonitor', async (req, res) => {
        res.sendStatus(200);
    });
};
