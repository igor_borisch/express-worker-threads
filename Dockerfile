FROM mhart/alpine-node:12
MAINTAINER alfabank

WORKDIR /app

COPY src ./src/
COPY package.json .
COPY node_modules ./node_modules/
COPY config ./config/

CMD npm start
EXPOSE 8080
