import request from 'request';

const CONFIG_SERVER_URL = process.env.config_server_url || 'http://ufrmsdev1/ufr-oncredit-settings';
const NODE_ENV = process.env.NODE_ENV || 'development';

const getSettings = () => new Promise((resolve, reject) => {
    const options = {
        url: `${CONFIG_SERVER_URL}/ufr-oncredit-commercial-offer-api/${NODE_ENV}`,
        method: 'GET'
    };
    const callback = (error, response, body) => {
        if (!error && response.statusCode === 200) {
            const settings = JSON.parse(body);

            resolve(settings);
        } else {
            reject(error);
        }
    };

    request(options, callback);
});

const setConfig = async () => {
    const settings = await getSettings();

    const wsEQhost = settings.propertySources[1].source['wsEQ.host'];
    const wsEQexternalSystemCode = settings.propertySources[1].source['wsEQ.externalSystemCode'];
    const wsEQexternalUserCode = settings.propertySources[1].source['wsEQ.externalUserCode'];
    const wsEQuserId = settings.propertySources[1].source['wsEQ.userId'];

    const wcfFileHost = settings.propertySources[1].source['wcf.fileHost'];
    const wcfUserName = settings.propertySources[1].source['wcf.userName'];
    const wcfPassword = settings.propertySources[1].source['wcf.password'];

    const obipHost = settings.propertySources[0].source['obip.host'];
    const obipUserId = settings.propertySources[0].source['obip.userId'];
    const obipPassword = settings.propertySources[0].source['obip.password'];
    const obipTemplatePath = settings.propertySources[0].source['obip.cmrclOfferTemplatePath'];

    const config = {
        serverPort: 5050,
        startMessage: 'server is running on port:',
        services: {
            saveFileService: {
                url: `http://${wcfFileHost}/WSFile.svc?wsdl`,
                method: 'POST',
                wcfUserName,
                wcfPassword
            },
            reportService: {
                url: `http://${obipHost}/xmlpserver/services/v2/ReportService?wsdl`,
                method: 'POST',
                obipUserId,
                obipPassword,
                obipTemplatePath
            },
            externalCompanyDataService: {
                url: `http://${wsEQhost}/CS/EQ/WSExternalCompanyData10/SOAP?wsdl`,
                method: 'POST',
                wsEQexternalSystemCode,
                wsEQexternalUserCode,
                wsEQuserId
            }
        }
    };

    return (config);

};

export default setConfig;
